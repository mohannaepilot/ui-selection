import React, { useState } from 'react';
import AntDesignWraper from './components/AntDesignWraper';
import FluentUIWraper from './components/FluentUIWraper';
import MaterialUIWraper from './components/MaterialUIWraper';

function App() {

  const [ui, setUi] = useState("material");

  const renderUI = () => {
    if (ui === "ant") {
      return <AntDesignWraper />;
    } else if (ui === "fluent") {
      return <FluentUIWraper />
    } else {
      return <MaterialUIWraper />;
    }
  }
  return (
    <div>
      <select value={ui} onChange={(newValue) => setUi(newValue.target.value)}>
        <option value="material">Material UI</option>
        <option value="ant">Ant Design</option>
        <option value="fluent">Fluent UI</option>
      </select>
      <hr />
      {renderUI()}

    </div>
  );
}

export default App;
