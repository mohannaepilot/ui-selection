
import { Checkbox, DatePicker, DayOfWeek, Dropdown, IDatePickerStrings, IDropdownOption, PrimaryButton } from '@fluentui/react';
import React from 'react';

const DayPickerStrings: IDatePickerStrings = {
    months: [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    ],
    shortMonths: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

    days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

    shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],

    goToToday: 'Go to today',
    prevMonthAriaLabel: 'Go to previous month',
    nextMonthAriaLabel: 'Go to next month',
    prevYearAriaLabel: 'Go to previous year',
    nextYearAriaLabel: 'Go to next year',
    closeButtonAriaLabel: 'Close date picker',
    monthPickerHeaderAriaLabel: '{0}, select to change the year',
    yearPickerHeaderAriaLabel: '{0}, select to change the month',
};

export default function FluentUIComponent() {
    const [firstDayOfWeek, setFirstDayOfWeek] = React.useState(DayOfWeek.Sunday);

    const onDropdownChange = (event: React.FormEvent<HTMLDivElement>, option: IDropdownOption) => {
        setFirstDayOfWeek((DayOfWeek as any)[option.key]);
    };

    return (
        <div style={{ padding: 20 }}>
            <p>FluentUIComponent</p>
            <PrimaryButton text="Primary" allowDisabledFocus />

            <br />
            <br />
            <Checkbox label="Unchecked checkbox (uncontrolled)" />
            <br />
            <Checkbox label="Indeterminate checkbox (uncontrolled)" defaultIndeterminate />
            <br />

            <DatePicker
                firstDayOfWeek={firstDayOfWeek}
                strings={DayPickerStrings}
                placeholder="Select a date..."
                ariaLabel="Select a date"
            />
            <Dropdown
                label="Select the first day of the week"
                options={[
                    {
                        text: 'Sunday',
                        key: DayOfWeek[DayOfWeek.Sunday],
                    },
                    {
                        text: 'Monday',
                        key: DayOfWeek[DayOfWeek.Monday],
                    },
                    {
                        text: 'Tuesday',
                        key: DayOfWeek[DayOfWeek.Tuesday],
                    },
                    {
                        text: 'Wednesday',
                        key: DayOfWeek[DayOfWeek.Wednesday],
                    },
                    {
                        text: 'Thursday',
                        key: DayOfWeek[DayOfWeek.Thursday],
                    },
                    {
                        text: 'Friday',
                        key: DayOfWeek[DayOfWeek.Friday],
                    },
                    {
                        text: 'Saturday',
                        key: DayOfWeek[DayOfWeek.Saturday],
                    },
                ]}
                selectedKey={DayOfWeek[firstDayOfWeek!]}
                // @ts-ignore eslint-disable-next-line react/jsx-no-bind
                onChange={onDropdownChange}
            />

        </div>
    );
}