
import React, { useState } from 'react';
import { Button, Checkbox, DatePicker, Select, TimePicker } from "antd";
import 'antd/dist/antd.css';

const { Option } = Select;



export default function AntDesignComponent() {
    const [type, setType] = useState('time');

    return (
        <div style={{padding: 20}}>
            <p>AntDesignComponent</p>
            <p><Button type="primary" style={{ marginLeft: 8 }}>Primary Button</Button></p>
            <br />
            <p><Checkbox>Checkbox</Checkbox></p>
            <br />
            

                <Select value={type} onChange={setType}>
                    <Option value="time">Time</Option>
                    <Option value="date">Date</Option>
                    <Option value="week">Week</Option>
                    <Option value="month">Month</Option>
                    <Option value="quarter">Quarter</Option>
                    <Option value="year">Year</Option>
                </Select>
                <PickerWithType type={type} onChange={(value: any) => console.log(value)} />

            

        </div>
    );
}

function PickerWithType(props: any) {
    if (props.type === 'time') return <TimePicker onChange={props.onChange} />;
    if (props.type === 'date') return <DatePicker onChange={props.onChange} />;
    return <DatePicker picker={props.type} onChange={props.onChange} />;
}