import { Button, Checkbox, FormControlLabel, FormGroup } from '@material-ui/core';
import React from 'react';


import {
    DateTimePicker,
} from '@material-ui/pickers';

export default function MaterialUIComponent() {

    const [state, setState] = React.useState({
        checkedA: true,
        checkedB: true,
        checkedF: true,
        checkedG: true,
    });

    const [selectedDate, setSelectedDate] = React.useState<Date | null>(
        new Date('2014-08-18T21:11:54'),
    );

    const handleDateChange = (date: Date | null) => {
        setSelectedDate(date);
    };
    const handleChange = (event: any) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    return (
        <div style={{padding: 20}}>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />

            <p>MaterialUIComponent</p>
            <Button variant="contained" color="primary">Hello World</Button>

            <FormGroup row>
                <FormControlLabel
                    control={<Checkbox checked={state.checkedA} onChange={handleChange} name="checkedA" />}
                    label="Secondary"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={state.checkedB}
                            onChange={handleChange}
                            name="checkedB"
                            color="primary"
                        />
                    }
                    label="Primary"
                />
                <FormControlLabel control={<Checkbox name="checkedC" />} label="Uncontrolled" />
                <FormControlLabel disabled control={<Checkbox name="checkedD" />} label="Disabled" />
                <FormControlLabel disabled control={<Checkbox checked name="checkedE" />} label="Disabled" />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={state.checkedF}
                            onChange={handleChange}
                            name="checkedF"
                            indeterminate
                        />
                    }
                    label="Indeterminate"
                />
            </FormGroup>

            <DateTimePicker value={selectedDate} onChange={handleDateChange} />


        </div>)
        ;
}