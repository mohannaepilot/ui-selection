import React, { useState } from 'react';
import MaterialUIComponent from './MaterialUIComponent';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { Switch } from '@material-ui/core';
import purple from '@material-ui/core/colors/purple';
import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';

export const lightTheme = createMuiTheme({
    palette: {
        primary: {
            main: purple[500],
        },
        secondary: {
            main: green[500],
        },
    },
});
export const darkTheme = createMuiTheme({
    palette: {
        primary: {
            main: red[500],
        },
        secondary: {
            main: blue[500],
        },
    },
});

export default function MaterialUIWraper() {
    const [useDarkMode, setUseDarkMode] = useState(false);

    return (
        <ThemeProvider theme={useDarkMode ? darkTheme : lightTheme}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Switch
                    checked={useDarkMode}
                    onChange={(event: any) => setUseDarkMode(event.target.checked)}
                    name="checkedA"
                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                />
                <MaterialUIComponent />
            </MuiPickersUtilsProvider>
        </ThemeProvider>

    );
}