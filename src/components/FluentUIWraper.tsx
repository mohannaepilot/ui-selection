
import React, { useState } from 'react';
import FluentUIComponent from './FluentUIComponent';
import { initializeIcons } from '@fluentui/react/lib/Icons';
import { loadTheme, Stack, Toggle } from '@fluentui/react';
import { ThemeProvider } from '@fluentui/react-theme-provider';

export const lightTheme = {
    palette: {
        themePrimary: '#128712',
        themeLighterAlt: '#f1faf1'
    }
};
export const darkTheme = {
    palette: {
        themePrimary: '#ff3404',
        themeLighterAlt: '#010801'
    }
};


/*loadTheme({
    palette: {
        themePrimary: '#ff78d4',
        themeLighterAlt: '#eff6fc',
        themeLighter: '#deecf9',
        themeLight: '#c7e0f4',
        themeTertiary: '#71afe5',
        themeSecondary: '#2b88d8',
        themeDarkAlt: '#106ebe',
        themeDark: '#005a9e',
        themeDarker: '#004578',
        neutralLighterAlt: '#f8f8f8',
        neutralLighter: '#f4f4f4',
        neutralLight: '#eaeaea',
        neutralQuaternaryAlt: '#dadada',
        neutralQuaternary: '#d0d0d0',
        neutralTertiaryAlt: '#c8c8c8',
        neutralTertiary: '#c2c2c2',
        neutralSecondary: '#858585',
        neutralPrimaryAlt: '#4b4b4b',
        neutralPrimary: '#333333',
        neutralDark: '#272727',
        black: '#1d1d1d',
        white: '#ffffff',
    },
});*/

initializeIcons();

export default function FluentUIWraper() {

    const [useDarkMode, setUseDarkMode] = useState(false);
    return (
        <ThemeProvider applyTo="body" theme={useDarkMode ? darkTheme : lightTheme}>
            <Stack>
                <Stack.Item>
                    <Toggle
                        label="Change themes"
                        onText="Dark Mode" offText="Light Mode"
                        onChange={() => setUseDarkMode(!useDarkMode)}
                    />
                </Stack.Item>
            </Stack>
            <FluentUIComponent />
        </ThemeProvider>

    );
}