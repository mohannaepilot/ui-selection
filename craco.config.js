const CracoLessPlugin = require('craco-less');

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: { 
                '@primary-color': '#ff0045',
                'link-color': '#1DA57A',
                'border-radius-base': '2px', 
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};